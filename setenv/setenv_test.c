#include<stdio.h>
#include<stdlib.h>


//###########################################################
//Get Environment Variable and concat a filename and search
//shkim - 22.7.29
//###########################################################

typedef unsigned char uint8;
typedef uint8* uint8_ptr;
typedef unsigned int uint32;

#define kdn_memcpy_s(dst,src,len) \
{\
	uint32 _kdn_memcpy_i;\
	uint8 *_kdn_memcpy_pdst = (uint8*) (dst);\
	uint8 *_kdn_memcpy_psrc = (uint8*) (src);\
	for ( _kdn_memcpy_i = 0; _kdn_memcpy_i < (len); _kdn_memcpy_i++) {\
		_kdn_memcpy_pdst[_kdn_memcpy_i] = _kdn_memcpy_psrc[_kdn_memcpy_i] ;\
	}\
}

// Same as strlen of <string.h>
uint32 kdn_strlen(uint8* ptr)
{
    if (ptr == NULL) return 0;
    else
		return (*ptr) ? kdn_strlen(++ptr)+1 : 0;
}

// String Concat
uint8_ptr kdn_strcat(uint8_ptr env, uint8_ptr file) 
{
    uint8_ptr ptr = env + kdn_strlen(env);
    while(*file != '\0') {
        *ptr++ = *file++;
    }
    *ptr= '\0';
    return env;
}


// String Concat Stable
uint32 kdn_set_nextenv2(uint8_ptr dest_env, uint8_ptr src_env, uint8_ptr* idx) 
{
		if (*idx == NULL)
		{                       
				if (src_env != NULL)
						*idx = src_env;
				else           
						return 0;
		}
#ifdef KDN_WIN
		else if(*(*idx) == ';') *(*idx)++;
#else
		else if(*(*idx) == ':') *(*idx)++;
#endif

		if(*(*idx) == '\0') return 0;
#ifdef KDN_WIN
		while(*(*idx) != '\0' && *(*idx) != ';'){
#else
				while(*(*idx) != '\0' && *(*idx) != ':'){
#endif
						*dest_env++ = *(*idx)++;
				}
				*dest_env= '\0';

				return 1;
		}

int main(int argc, char *argv[]) {
    uint8_ptr ptr_env;
    uint8_ptr filename = "KDN_LIB.h";
    uint8_ptr env;
    uint8_ptr location;
    uint8_ptr idx = NULL;

    FILE *fr;
    int j;

    ptr_env = getenv("KDNLIBH");
    //printf("size = %d\n", sizeof(ptr_env)+sizeof(filename));
    env = (uint8_ptr)calloc(sizeof(ptr_env) * kdn_strlen(ptr_env), sizeof(uint8));
	location = (uint8_ptr)calloc(sizeof(ptr_env) * kdn_strlen(ptr_env)
			+ sizeof(filename) * kdn_strlen(filename) + 8, sizeof(uint8));
    
    printf("size = %ld\n", sizeof(ptr_env)*kdn_strlen(ptr_env) 
		    + sizeof(filename) * kdn_strlen(filename)+8);
    //printf("ptr_env : %s\n", ptr_env);
    printf("ptr_env length : %d\n", kdn_strlen(ptr_env));
    printf("filename length : %d\n", kdn_strlen(filename));

    fr = fopen(env, "r");
	while(kdn_set_nextenv2(env, ptr_env, &idx) && (fr==NULL))
	{
		kdn_strcat(env, "/");
		kdn_strcat(env, filename);
		fr = fopen(env, "r");
        fprintf(stdout, "FOO : %s\n", env);
	}

    //fr = fopen(env, "r");
    if(!fr) printf("File not found\n");

    free(env);
    return 0;
}

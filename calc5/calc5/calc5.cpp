#include <stdio.h>

int fsum(int num1, int num2){
	return num1 + num2;
}

int fsub(int num1, int num2){
	return num1 - num2;
}

int fmult(int num1, int num2){
	return num1 * num2;
}

float fdiv(float num1, float num2){
	return num1 / num2;
}

int main(){
	int num1 = 0;
	int num2 = 0;
	int sum = 0;
	int sub = 0;
	int mult = 0;
	float div = 0;

	int userInput = 0;

	while(1){
		printf("수행할 연산을 선택하세요\n");
		printf("1. Sum\n");
		printf("2. Sub\n");
		printf("3. Mult\n");
		printf("4. Div\n");
		printf("=> ");
		scanf("%d", &userInput);

	    if(userInput < 0 || userInput > 4){
			printf("잘못입력하셨습니다\n");
			return 0;
		}	

		printf("연산을 수행할 두 값을 넣어주세요\n");
		printf("input1: \n");
		printf("=> ");
		scanf("%d", &num1);
		printf("input2: \n");
		printf("=> ");
		scanf("%d", &num2);

		printf("=============== result ================= \n");
		if (userInput == 1)
		{
			sum = fsum(num1, num2);
			printf("Sum : %d\n", sum);
		}
		else if (userInput == 2)
		{
			sub = fsub(num1, num2);
			printf("Substraction : %d\n", sub);
		}
		else if (userInput == 3)
		{
			mult = fmult(num1, num2);
			printf("Multiplication : %d\n", mult);
		}
		else if (userInput == 4)
		{
			div = fdiv(num1, num2);
			printf("Division : %.1f\n", div);
		}
		else{
			printf("잘못입력하셨습니다\n");
		}
	}
	return 0;
}
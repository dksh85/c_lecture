#include<stdio.h>

void merge(int ar[], int p, int q, int r){
	int i = p; int j = q+1; int k = p;
	int temp[10];
	while( i <= q && j <= r){
		if(ar[i] > ar[j]) temp[k++] = ar[j++];
		else temp[k++] = ar[i++];
	}
	while(i <= q) temp[k++] = ar[i++];
	while(j <= r) temp[k++] = ar[j++];
	for (int a = p; a<=r; a++) ar[a] = temp[a];
}

void mergeSort(int ar[], int p, int r){
	int q = 0;
	if(p < r){
		q = (p+r)/2;
		mergeSort(ar, p, q);
		mergeSort(ar, q+1, r);
		merge(ar, p, q, r);
	}
}



int main(){
	int ar1[] = { 5,4,6,8,2,1,9,7,3,0 };
	int res1[10];

	int temp[10];
	int oddIdx = 0;
	int evenIdx = 5;
	int oddmin = -1;
	int oddmax = -1;

	mergeSort(ar1, 0, 9);
	/*
	printf("res1 = ");
	for (int i = 0; i < 10; i++){
		printf("%d ", ar1[i]);
	}
	printf("\n");
	*/

	for (int i = 0; i < 10; i++){
		if (ar1[i] % 2 == 1) res1[oddIdx++] = ar1[i];
	    else res1[evenIdx++] = ar1[i];
	}

	printf("res1 = ");
	for (int i = 0; i < 10; i++){
		printf("%d ", res1[i]);
	}
	printf("\n");
}
#include <stdio.h>

int main(){
	int num1 = 0;
	int num2 = 0;
	int sum = 0;
	int sub = 0;
	int mult = 0;
	float div = 0;

	printf("input1: \n");
	scanf("%d", &num1);
	printf("input2: \n");
	scanf("%d", &num2);

	sum = num1 + num2;
	sub = num1 - num2;
	mult = num1 * num2;

	div = (float)num1/(float)num2;

	printf("=============== result ================= \n");
	printf("Sum : %d\n", sum);
	printf("Substraction : %d\n", sub);
	printf("Multiplication : %d\n", mult);
	printf("Division : %.1f\n", div);
	
	return 0;
}